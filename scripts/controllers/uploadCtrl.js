(function () {

    var uploadCtrl = function ($scope, UploadService) {
        $scope.uploadedFile = function(element) {
            $scope.$apply(function($scope) {
                $scope.files = element.files;
            });
        };

        $scope.addFile = function() {
            UploadService.uploadfile($scope.files,
                function( msg ) // success
                {
                    console.log('uploaded');
                },
                function( msg ) // error
                {
                    console.log('error');
                });
        };

    };
    APP.controller('uploadCtrl',uploadCtrl);

}());