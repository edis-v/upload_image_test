(function () {

    var UploadService = function ($http) {

        this.uploadfile = function(files,success,error){

            var fd = new FormData();

            var url = 'http://localhost:3000/api/v1/images';

            angular.forEach(files,function(file){
                fd.append('file',file);
            });

            //sample data
            var data ={
                name : "Cover.jpg",
                type : "picture"
            };

            fd.append("data", JSON.stringify(data));

            $http.post(url, fd, {
                withCredentials : false,
                headers : {
                    'Content-Type' : undefined
                },
                transformRequest : angular.identity
            })
                .success(function(data)
                {
                    console.log(data);
                })
                .error(function(data)
                {
                    console.log(data);
                });

        }

    };
    APP.service('UploadService',UploadService);
}());